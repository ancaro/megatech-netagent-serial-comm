# -*- coding: utf-8 -*-

# This class implements a very simple RS232 serial
# communication protocol with Megatech's NetAgent 
# SNMP embedded server. This is for integration 
# purposes; you can send UPS's state data to the
# Megatech's device with very few effort. To know UPS's
# state, this module uses Redis key-value datastore to 
# communicate with another program that is actually 
# reading UPS state data in real time, and 'publishing'
# this data into predefined, customizable Redis channels.
# An importable python class for making this UPS state data
# publishing task more simple is also provided.

import datetime
import serial
import redis

class Menasco:

	_empty_request = ''
	_Q1 = 'Q1\r'
	_Q2 = 'Q2\r'
	_Q3 = 'Q3\r'
	_F = 'F\r'
	_I = 'I\r'

	def __init__(self, serialPort='/dev/ttyUSB0', baudrate=2400, timeout=20, write_timeout=10, debug_mode=False):
		self.debugMode = debug_mode
		self.serialPort = serial.Serial( serialPort, baudrate, timeout=timeout, write_timeout=write_timeout )
	
	def _isPortOpen(self):
		return self.serialPort.isOpen()

	def _inWaiting(self):
		return self.serialPort.in_waiting

	def _outWaiting(self):
		return self.serialPort.out_waiting

	def _readSerialPort(self):
		return self.serialPort.read()

	def _closeSerialPort(self):
		self.serialPort.close()

	def _resetSerialInputBuffer(self):
		self.serialPort.reset_input_buffer()

	def _resetSerialOutputBuffer(self):
		self.serialPort.reset_output_buffer()

	def _resetSerialBuffer(self):
		self._resetSerialInputBuffer()
		self._resetSerialOutputBuffer()

	def _debugLog(self, data_received):
		timestamp = datetime.datetime.now()
		return "[%s:%s:%s.%s][Received]: '%s'" % (timestamp.hour, timestamp.minute, timestamp.second, timestamp.microsecond, data_received)

	def _readSerialRequest(self, request_type):
		
		# Read serial port until request_type is received or
		# timeout is reached.
		serial_data_readed = self.serialPort.read_until( request_type )

		# Get last request readed from serial buffer, in case
		# there are more than one.
		if request_type == self._Q1:
			serial_request = serial_data_readed[-3:]
		else:
			serial_request = serial_data_readed[-2:]

		# If no request received.
		if serial_request == self._empty_request:
			
			if self.debugMode:
				print self._debugLog('')

			return False # Negative response, not what expected.

		# If request_type is received in serial port.
		elif serial_request == request_type:
			
			if self.debugMode:
				print self._debugLog(request_type)

			return True # Positive response, is what expected.

		# Data received, but is not the request_type.
		else:

			if self.debugMode:
				print self._debugLog('Unknown')

			return False # Negative response, not what expected.


def main():

	timestamp = datetime.datetime.now()
	print "[%s:%s:%s.%s][Start]" % (timestamp.hour, timestamp.minute, timestamp.second, timestamp.microsecond)

	MANUFACTURER_INFO = "#ATSEI           UPS_MODEL  ATSEI LTDA\r"

	ups_data_store = redis.StrictRedis(host='localhost', port=6379)

	megatech_device = Menasco(debug_mode=True)
	megatech_device._resetSerialBuffer()

	while True:
		
		# Wait for 'Q1' serial request.
		if megatech_device._readSerialRequest( megatech_device._Q1 ):
			# Read Q1 variables
			# EEE.0 = Voltaje de entrada de la fase i.
			# MMM.0 = Voltaje de falla. (Fault_voltage = 140)
			# SSS.0 = Voltaje de salida de la fase i.
			# CCC = Porcentaje de corriente de la fase i (Con respecto a la máxima corriente = 100).
			# FF.0 = Frecuencia de la red.
			# BB.0 = Porcentaje de voltaje en batería con respecto a 6_Por_NBaterías = 83.
			# TT.0 = Temperatura en fase i.
			# RLYX = Bits de Falla_Red, Low_Battery, Baypass_In, Múltiple_X, Respectivamente.
			eee_1 = ups_data_store.get('eee_1') or '000.0'
			mmm_1 = ups_data_store.get('mmm_1') or '140.0'
			sss_1 = ups_data_store.get('sss_1') or '000.0'
			ccc_1 = ups_data_store.get('ccc_1') or '100'
			ff_1 = ups_data_store.get('ff_1') or '60.0'
			bb_1 = ups_data_store.get('bb_1') or '83.0'
			tt_1 = ups_data_store.get('tt_1') or '00.0'
			rlxy_1 = ups_data_store.get('rlxy_1') or '0011'

			# Response to Q1 request
			# (EEE.0 MMM.0 SSS.0 CCC FF.0 BB.0 TT.0 RLYX0000<Cr>
			Q1_response = "(%s %s %s %s %s %s %s %s0000\r" % (eee_1, mmm_1, sss_1, ccc_1, ff_1, bb_1, tt_1, rlxy_1)
			# Then, send response.
			megatech_device.serialPort.write(Q1_response)
		

		# Wait for 'I' serial request.
		if megatech_device._readSerialRequest( megatech_device._I ):
			# Then, send response.
			megatech_device.serialPort.write( MANUFACTURER_INFO )
		

		# Wait for 'F' serial request.
		if megatech_device._readSerialRequest( megatech_device._F ):
			# Read rating information
			# EEE.0 = Voltaje de la fase 1.
			# MMM = Máxima corriente del equipo = 100.
			# BB.B0 = Voltaje nominal de las baterías = 12.00.
			# FF.0 = Frecuencia nominal del equipo = 60.0.

			eee_r = ups_data_store.get('eee_r') or '000.0'
			mmm_r = ups_data_store.get('mmm_r') or '100'
			bb_r = ups_data_store.get('bb_r') or '12.00'
			ff_r = ups_data_store.get('ff_r') or '60.0'

			# Response to F request
			# #EEE.0 MMM BB.B0 FF.0<Cr>
			F_response = "#%s %s %s %s\r" % (eee_r, mmm_r, bb_r, ff_r)

			# Then, send response.
			megatech_device.serialPort.write(F_response)


if __name__ == '__main__' :
	main()
